import bson

from abstract_attract_test import AbstractAttractTest

SVN_SERVER_URL = 'svn://biserver/agent327'


class ShortcodeTest(AbstractAttractTest):
    def setUp(self, **kwargs):
        AbstractAttractTest.setUp(self, **kwargs)

        self.mngr = self.attract.task_manager
        self.proj_id, self.project = self.ensure_project_exists()

    def test_increment_simple(self):
        from attract import shortcodes

        with self.app.test_request_context():
            code = shortcodes.generate_shortcode(self.proj_id, 'jemoeder', 'ø')
        self.assertEqual('ø1', code)

        with self.app.test_request_context():
            code = shortcodes.generate_shortcode(self.proj_id, 'jemoeder', 'č')
        self.assertEqual('č2', code)

    def test_multiple_projects(self):
        from attract import shortcodes

        proj_id2, project2 = self.ensure_project_exists(project_overrides={
            '_id': bson.ObjectId(24 * 'f'),
            'url': 'proj2',
        })

        with self.app.app_context():
            code1 = shortcodes.generate_shortcode(self.proj_id, 'jemoeder', 'č')
            code2 = shortcodes.generate_shortcode(proj_id2, 'jemoeder', 'č')
            code3 = shortcodes.generate_shortcode(proj_id2, 'jemoeder', 'č')
        self.assertEqual('č1', code1)
        self.assertEqual('č1', code2)
        self.assertEqual('č2', code3)
