# Attract people management extension for Pillar

This project contains Attract, a task management extension for the Pillar
platform.

## Development

Dependencies are managed via [Poetry](https://poetry.eustace.io/).

```
git clone git@git.blender.org:pillar-python-sdk.git ../pillar-python-sdk
git clone git@git.blender.org:pillar.git ../pillar
pip install -U --user poetry
poetry install
```
