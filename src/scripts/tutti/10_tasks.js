/**
 * Create a asset and show it in the #item-details div.
 */
function thenCreateAsset(project_url)
{
    if (project_url === undefined) {
        throw new ReferenceError("asset_create(" + project_url+ ") called.");
    }
    var url = '/attract/' + project_url + '/assets/create';

    data = {
        project_url: project_url
    };

    return $.post(url, data, function(asset_data) {
        pillar.events.Nodes.triggerCreated(asset_data);
        return asset_data;
    })
    .fail(function(xhr) {
        if (console) {
            console.log('Error creating asset');
            console.log('XHR:', xhr);
        }
        $('#item-details').html(xhr.responseText);
    });
}


/**
 * Create a task and show it in the #item-details div.
 *
 * 'shot_id' may be undefined, in which case the task will not
 * be attached to a shot.
 */
function thenCreateTask(shot_id, task_type)
{
    if (task_type === undefined) {
        throw new ReferenceError("task_create(" + shot_id + ", " + task_type + ") called.");
    }

    var project_url = ProjectUtils.projectUrl();
    var url = '/attract/' + project_url + '/tasks/create';
    var has_shot_id = typeof shot_id !== 'undefined';

    data = {
        task_type: task_type,
    };
    if (has_shot_id) data.parent = shot_id;

    return $.post(url, data, function(task_data) {
        if (console) console.log('Task created:', task_data);
        pillar.events.Nodes.triggerCreated(task_data);
        return task_data;
    })
    .fail(function(xhr) {
        if (console) {
            console.log('Error creating task');
            console.log('XHR:', xhr);
        }
        $('#item-details').html(xhr.responseText);
    });
}

var save_on_ctrl_enter = ['shot', 'asset', 'task'];
$(document).on('keyup', function(e){
    if ($.inArray(save_on_ctrl_enter, ProjectUtils.context())) {
        var active = document.activeElement;

        // Save on Ctrl + Enter anytime except when comment field is on focus
        if (active.id != 'comment_field'){
            if ((e.keyCode == 10 || e.keyCode == 13) && e.ctrlKey){
                $("#item-save").trigger( "click" );
            }
        }
    }
});
