const TEMPLATE =`
<a  class="task"
    :class="taskClass"
    :href="taskLink"
    :title="taskTitle"
    @click.prevent.stop="$emit('item-clicked', arguments[0], task.getId())"
/>
`;

let CellTasksLink = Vue.component('attract-cell-task-link', {
    template: TEMPLATE,
    props: {
        task: Object,
        itemType: String,
    },
    computed: {
        taskClass() {
            let classes = {'active': this.task.isSelected};
            classes[`status-${this.task.getProperties().status}`] = true;
            return classes;
        },
        taskLink() {
            let project_url = ProjectUtils.projectUrl();
            return `/attract/${project_url}/${this.itemType}/with-task/${this.task.getId()}`;
        },
        taskTitle() {
            let status = (this.task.getProperties().status || '').replace('_', ' ');
            return `Task: ${this.task.getName()}\nStatus: ${status}`
        },
    },
});

export { CellTasksLink }
