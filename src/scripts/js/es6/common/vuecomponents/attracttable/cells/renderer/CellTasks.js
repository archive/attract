let CellDefault = pillar.vuecomponents.table.cells.renderer.CellDefault;
import './CellTasksLink'

const TEMPLATE =`
<div>
    <div class="tasks">
        <attract-cell-task-link
            v-for="t in tasks"
            :task="t"
            :itemType="itemType"
            :key="t._id"
            @item-clicked="$emit('item-clicked', ...arguments)"
        />
    </div>
    <button class="add-task-link"
        v-if="canAddTask"
        @click.prevent.stop="onAddTask"
    >
        <i class="pi-plus">Task</i>
    </button>
</div>
`;

let CellTasks = Vue.component('attract-cell-tasks', {
    extends: CellDefault,
    template: TEMPLATE,
    computed: {
        tasks() {
            return this.rawCellValue;
        },
        canAddTask() {
            if (this.tasks.length < 1 ) {
                let projectId = ProjectUtils.projectId();
                return attract.auth.AttractAuth.canUserCreateTask(projectId);
            }
            return false;
        },
        itemType() {
            let node_type = this.rowObject.underlyingObject.node_type;
            return node_type.replace('attract_', '') + 's'; // eg. attract_asset to assets
        }
    },
    methods: {
        onAddTask(event) {
            thenCreateTask(this.rowObject.getId(), this.column.taskType)
            .then((task) => {
                this.$emit('item-clicked', event, task._id);
            });
        },
    },
});

export { CellTasks }
