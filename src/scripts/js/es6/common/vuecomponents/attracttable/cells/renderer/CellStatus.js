let CellDefault = pillar.vuecomponents.table.cells.renderer.CellDefault;

let CellStatus = Vue.component('attract-cell-Status', {
    extends: CellDefault,
    computed: {
        cellValue() {
            return '';
        },
    },
});

export { CellStatus }
