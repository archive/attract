let CellDefault = pillar.vuecomponents.table.cells.renderer.CellDefault;

const TEMPLATE =`
<div>
    <a
        @click="ignoreDefault" 
        :href="cellLink"
        :title="cellValue"
    >
        {{ cellValue }}
    </a>
</div>
`;

let CellRowObject = Vue.component('pillar-cell-row-object', {
    extends: CellDefault,
    template: TEMPLATE,
    computed: {
        cellLink() {
            let project_url = ProjectUtils.projectUrl();
            let item_type = this.itemType();
            return `/attract/${project_url}/${item_type}s/${this.rowObject.getId()}`;
        }
    },
    methods: {
        itemType() {
            let node_type = this.rowObject.underlyingObject.node_type;
            return node_type.replace('attract_', ''); // eg. attract_task to tasks
        },
        ignoreDefault(event) {
            // Don't follow link, let the event bubble and the row handles it
            event.preventDefault();
        }
    },
});

export { CellRowObject }
