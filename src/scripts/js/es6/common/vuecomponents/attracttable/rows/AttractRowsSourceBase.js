let RowObjectsSourceBase = pillar.vuecomponents.table.rows.RowObjectsSourceBase;

/**
 * Base for all attract tables. Listens to events on create/delete events and keeps the the source up to date
 * accordingly.
 */
class AttractRowsSourceBase extends RowObjectsSourceBase {
    constructor(projectId, node_type, rowClass) {
        super();
        this.projectId = projectId;
        this.node_type = node_type;
        this.rowClass = rowClass;
    }

    createRow(node) {
        let row = new this.rowClass(node);
        this.registerListeners(row);
        return row;
    }

    initRowObjects(nodes) {
        this.rowObjects = nodes.map(this.createRow.bind(this));
        pillar.events.Nodes.onCreated(this.node_type, this.onNodeCreated.bind(this));
    }

    registerListeners(rowObject) {
        pillar.events.Nodes.onDeleted(rowObject.getId(), this.onNodeDeleted.bind(this));
    }

    onNodeDeleted(event) {
        this.rowObjects = this.rowObjects.filter((rowObj) => {
            return rowObj.getId() !== event.detail;
        });
    }

    onNodeCreated(event) {
        let rowObj = this.createRow(event.detail);
        rowObj.thenInit();
        this.rowObjects = this.rowObjects.concat(rowObj);
    }
}

export { AttractRowsSourceBase }
