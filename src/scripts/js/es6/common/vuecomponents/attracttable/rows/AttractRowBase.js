let RowBase = pillar.vuecomponents.table.rows.RowBase;

class AttractRowBase extends RowBase {
    constructor(underlyingObject) {
        super(underlyingObject);
        pillar.events.Nodes.onUpdated(this.getId(), this.onRowUpdated.bind(this));
    }

    onRowUpdated(event) {
        this.underlyingObject = event.detail;
    }

    getStatus() {
        return this.underlyingObject.properties.status;
    }
}

export { AttractRowBase }
