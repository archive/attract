import { TaskRow } from '../../taskstable/rows/TaskRow'
/**
 * Helper class that listens to events triggered when a RowObject task is updated/created/deleted and keep the tasks
 * array of a RowObject up to date accordingly.
 */

export class TaskEventListener {
    constructor(rowWithTasks) {
        this.rowObject = rowWithTasks;
    }

    register() {
        pillar.events.Nodes.onParentCreated(this.rowObject.getId(), 'attract_task', this.onTaskCreated.bind(this));
        this.rowObject.tasks.forEach(this.registerEventListeners.bind(this));
    }

    registerEventListeners(task) {
        pillar.events.Nodes.onUpdated(task.getId(), this.onTaskUpdated.bind(this));
        pillar.events.Nodes.onDeleted(task.getId(), this.onTaskDeleted.bind(this));
    }

    onTaskCreated(event) {
        let task = new TaskRow(event.detail);
        task.thenInit();
        this.registerEventListeners(task);
        this.rowObject.tasks = this.rowObject.tasks.concat(task);
    }

    onTaskUpdated(event) {
        let updatedTask = event.detail;
        for (const task of this.rowObject.tasks) {
            if (task.getId() === updatedTask._id) {
                task.underlyingObject = updatedTask;
                break;
            }
        }
    }

    onTaskDeleted(event) {
        this.rowObject.tasks = this.rowObject.tasks.filter((t) => {
            return t.getId() !== event.detail;
        });
    }
}
