import {CellStatus} from '../cells/renderer/CellStatus'
let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;

export class Status extends ColumnBase {
    constructor() {
        super('', 'attract-status');
        this.isMandatory = true;
    }
    getCellRenderer(rowObject) {
        return CellStatus.options.name;
    }
    getRawCellValue(rowObject) {
        return rowObject.getProperties().status;
    }
    getCellTitle(rawCellValue, rowObject) {
        function capitalize(str) {
            if(str.length === 0) return str;
            return str.charAt(0).toUpperCase() + str.slice(1);
        }
        let formatedStatus = capitalize(rawCellValue).replace('_', ' ');
        return `Status: ${formatedStatus}`;
    }
    getCellClasses(rawCellValue, rowObject) {
        let classes = super.getCellClasses(rawCellValue, rowObject);
        classes[`status-${rawCellValue}`] = true;
        return classes;
    }
    compareRows(rowObject1, rowObject2) {
        let sortNbr1 = this.getSortNumber(rowObject1);
        let sortNbr2 = this.getSortNumber(rowObject2);
        if (sortNbr1 === sortNbr2) return 0;
        return sortNbr1 < sortNbr2 ? -1 : 1;
    }
    getSortNumber(rowObject) {
        let statusStr = rowObject.getProperties().status;
        switch (statusStr) {
            case 'on_hold':     return 10;
            case 'todo':        return 20;
            case 'in_progress': return 30;
            case 'review':      return 40;
            case 'cbb':         return 50;
            case 'approved':    return 60;
            case 'final':       return 70;
            default:            return 9999; // invalid status
        }
    }
}
