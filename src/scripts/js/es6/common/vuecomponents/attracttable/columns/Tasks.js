let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;
import { CellTasks } from '../cells/renderer/CellTasks'

export class TaskColumn extends ColumnBase {
    constructor(taskType, columnType) {
        super(taskType, columnType);
        this.taskType = taskType;
    }

    getCellRenderer(rowObject) {
        return CellTasks.options.name;
    }

    getRawCellValue(rowObject) {
        return rowObject.getTasksOfType(this.taskType);
    }

    compareRows(rowObject1, rowObject2) {
        let numTasks1 = this.getRawCellValue(rowObject1).length;
        let numTasks2 = this.getRawCellValue(rowObject2).length;
        if (numTasks1 === numTasks2) return 0;
        return numTasks1 < numTasks2 ? -1 : 1;
    }

    getColumnClasses() {
        let classes = super.getColumnClasses();
        classes[this.taskType] = true;
        return classes;
    }
}
