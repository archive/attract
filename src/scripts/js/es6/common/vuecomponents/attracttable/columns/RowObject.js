let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;
import { CellRowObject } from '../cells/renderer/CellRowObject'

class RowObject extends ColumnBase {
    constructor() {
        super('Name', 'row-object');
        this.isMandatory = true;
    }

    getCellRenderer(rowObject) {
        return CellRowObject.options.name;
    }

    getRawCellValue(rowObject) {
        return rowObject.getName() || '<No Name>';
    }
}

export { RowObject }
