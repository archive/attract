let DateColumnBase = pillar.vuecomponents.table.columns.DateColumnBase;

function firstDate(prevDate, task) {
    let candidate = task.properties.due_date;
    if (prevDate && candidate) {
        if (prevDate !== candidate) {
            return new Date(candidate) < new Date(prevDate) ? candidate : prevDate;
        }
    }
    return prevDate || candidate;
}

function lastDate(prevDate, task) {
    let candidate = task.properties.due_date;
    if (prevDate && candidate) {
        if (prevDate !== candidate) {
            return new Date(candidate) > new Date(prevDate) ? candidate : prevDate;
        }
    }
    return prevDate || candidate;
}

function nextDate(prevDate, task) {
    let candidate = task.properties.due_date;
    if(candidate && new Date(candidate) >= new Date()) {
        return firstDate(prevDate, task);
    }
    return prevDate;
}

class DueDate extends DateColumnBase {
    getCellClasses(dueDate, rowObject) {
        let classes = super.getCellClasses(dueDate, rowObject);
        let isPostDueDate = false;
        if (dueDate) {
            isPostDueDate = new Date(dueDate) < new Date();
        }
        classes['warning'] = isPostDueDate;
        return classes;
    }
}

export class FirstTaskDueDate extends DueDate {
    constructor() {
        super('First Due Date', 'first-duedate');
    }
    getRawCellValue(rowObject) {
        let tasks = (rowObject.tasks || []).map(task => task.underlyingObject);
        return tasks.reduce(firstDate, undefined) || '';
    }
}

export class LastTaskDueDate extends DueDate {
    constructor() {
        super('Last Due Date', 'last-duedate');
    }
    getRawCellValue(rowObject) {
        let tasks = (rowObject.tasks || []).map(task => task.underlyingObject);
        return tasks.reduce(lastDate, undefined) || '';
    }
}

export class NextTaskDueDate extends DueDate {
    constructor() {
        super('Next Due Date', 'next-duedate');
    }
    getRawCellValue(rowObject) {
        let tasks = (rowObject.tasks || []).map(task => task.underlyingObject);
        return tasks.reduce(nextDate, undefined) || '';
    }
}

export class TaskDueDate extends DueDate {
    constructor() {
        super('Due Date', 'duedate');
    }
    getRawCellValue(rowObject) {
        let task = rowObject.getTask();
        return task.properties.due_date || '';
    }
}
