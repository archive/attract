const TEMPLATE =`
<div class="attract-box multiple-types">
    Objects of different types selected
</div>
`;

/**
 * For when objects of different node_type is selected.
 */
let MultipleTypes = Vue.component('attract-editor-multiple-types', {
    template: TEMPLATE,
});

export {MultipleTypes}
