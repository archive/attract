import { Empty } from './Empty'
import { MultipleTypes } from './MultipleTypes'
import { AssetEditor } from '../editor/AssetEditor'
import { TaskEditor } from '../editor/TaskEditor'
import { ShotEditor } from '../editor/ShotEditor'
import '../activities/Activities'

const TEMPLATE =`
<div class="attract-detailed-view">
    <div class="col_header">
        <span class="header_text">
            {{ headerText }}
            <i 
                v-if="isMultiItemsView"
                class="pi-link"
                title="Multiple items selected"
            />
        </span>
    </div>
    <component
        :is="editorType"
        :items="items"
        :project="project"
        :contextType="contextType"
        @objects-are-edited="$emit('objects-are-edited', ...arguments)"
        @saved-items="activitiesIsOutdated"
    />
    <attract-activities
        v-if="isSingleItemView"
        :objectId="singleObjectId"
        :outdated="isActivitiesOutdated"
        @activities-updated="activitiesIsUpToDate"
    />
    <comments-tree
        v-if="isSingleItemView"
        :parentId="singleObjectId"
        @new-comment="activitiesIsOutdated"
    />
</div>
`;

Vue.component('attract-detailed-view', {
    template: TEMPLATE,
    props: {
        items: Array,
        project: Object,
        contextType: String
    },
    data() {
        return {
            isActivitiesOutdated: true
        }
    },
    computed: {
        headerText() {
            switch(this.items.length) {
                case 0: return 'Details';
                case 1: return `${this.itemsTypeFormated} Details`
                default: return `${this.itemsTypeFormated} Details (${this.items.length})`;
            }
        },
        itemsType() {
            let itemsType = this.items.reduce((prevType, it) => {
                if(prevType) {
                    return prevType === it.node_type ? prevType : 'multiple_types';
                }
                return it.node_type;
            }, null);

            return itemsType || 'empty';
        },
        itemsTypeFormated() {
            return this.itemsType.replace('attract_', '').replace('multiple_types', '');
        },
        editorType() {
            if(!this.project) {
                return Empty.options.name;
            }
            switch(this.itemsType) {
                case 'attract_asset': return AssetEditor.options.name;
                case 'attract_shot': return ShotEditor.options.name;
                case 'attract_task': return TaskEditor.options.name;
                case 'multiple_types': return MultipleTypes.options.name;
                case 'empty': return Empty.options.name;
                default: 
                    console.log('No editor for:', this.itemsType);
                    return Empty.options.name;
            }
        },
        isMultiItemsView() {
            return this.items.length > 1;
        },
        isSingleItemView() {
            return this.items.length === 1;
        },
        singleObjectId() {
            return this.isSingleItemView ? this.items[0]._id : '';
        },
    },
    methods: {
        activitiesIsOutdated() {
            this.isActivitiesOutdated = true;
        },
        activitiesIsUpToDate() {
            this.isActivitiesOutdated = false
        }
    },
});
