const TEMPLATE =`
<div class="attract-box item-details-empty">Select Something</div>
`;

/**
 * For when nothing is selected in the table
 */
let Empty = Vue.component('attract-editor-empty', {
    template: TEMPLATE,
});

export {Empty}
