import './base/TextArea'
import './base/ConclusiveMark'
import {EditorBase, BaseProps} from './base/EditorBase'

const TEMPLATE =`
<div class="attract-box asset with-status"
    :class="editorClasses"
>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="nameProp"
        />
        <input class="item-name" name="name" type="text" placeholder="Asset Name"
            :disabled="!canEdit"
            :class="classesForProperty(nameProp)"
            v-model="nameProp.value"/>
        <button class="copy-to-clipboard btn item-id" name="Copy to Clipboard" type="button" title="Copy ID to clipboard"
            v-if="!isMultpleItems"
            :data-clipboard-text="items[0]._id"
        >
            ID
        </button>
    </div>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="descriptionProp"
        />
        <attract-editor-text-area
            placeholder="Description"
            :disabled="!canEdit"
            :class="classesForProperty(descriptionProp)"
            v-model="descriptionProp.value"
        />
    </div>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="statusProp"
        />
        <label for="item-status">Status:</label>
        <select class="input-transparent" id="item-status" name="status"
            :class="classesForProperty(statusProp)"
            :disabled="!canEdit"
            v-model="statusProp.value"
            >
            <option value=undefined disabled="true" v-if="!statusProp.isConclusive()"> *** </option>

            <option v-for="it in allowedStatusesPair"
                :key="it.id"
                :value="it.id"
            >{{it.text}}</option>
        </select>
    </div>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="notesProp"
        />
        <attract-editor-text-area
            placeholder="Notes"
            :class="classesForProperty(notesProp)"
            :disabled="!canEdit"
            v-model="notesProp.value"
        />
    </div>
    <div class="input-group-separator"/>
    <div class="input-group"
        v-if="canEdit"
    >
        <button class="btn btn-outline-success btn-block" id="item-save" type="submit"
            @click="save"
            :disabled="isSaving"
        >
            <i class="pi-check"/>Save Asset
        </button>
    </div>
</div>
`;


const AllProps = Object.freeze({
    ...BaseProps,
    PROP_NOTES: 'properties.notes',
});

let ALL_PROPERTIES = [];

for (const key in AllProps) {
    ALL_PROPERTIES.push(AllProps[key]);
}

let AssetEditor = Vue.component('attract-editor-asset', {
    template: TEMPLATE,
    extends: EditorBase,
    data() {
        return {
            multiEditEngine: this.createEditorEngine(ALL_PROPERTIES),
        }
    },
    watch: {
        items() {
            this.multiEditEngine = this.createEditorEngine(ALL_PROPERTIES);
        },
    },
    computed: {
        notesProp() {
            return this.multiEditEngine.getProperty(AllProps.PROP_NOTES);
        },
    },
});

export {AssetEditor}
