/**
 * Wrapper around jquery select2. Heavily inspired by: https://vuejs.org/v2/examples/select2.html
 */

let TEMPLATE = `
<div class="input-group attract-select2">
    <select multiple="" ref="select2" style="display: none;" id="apa"
        :disabled="disabled"
    >
        <slot/>
    </select>
</div>
`;

Vue.component('attract-select2', {
    props: {
        options: Array,
        value: Array,
        disabled: {
            type: Boolean,
            default: false
        }
    },
    template: TEMPLATE,
    mounted: function () {
        this.$nextTick(this.initSelect2);
    },
    watch: {
      value(value) {
          // update value
          $(this.$refs.select2)
            .val(value)
            .trigger('change.select2');
      },
      options(options) {
          // update options
          $(this.$refs.select2).empty().select2({ data: options });
      }
    },
    beforeDestroy() {
        $(this.$refs.select2).off().select2('destroy');
    },
    methods: {
        initSelect2() {
            $(this.$refs.select2)
                // init select2
                .select2({ data: this.options })
                .val(this.value)
                .trigger('change.select2')
                // emit event on change.
                .on('change', () => {
                    this.$emit('input', $(this.$refs.select2).val());
                });
        }
    },
  })
