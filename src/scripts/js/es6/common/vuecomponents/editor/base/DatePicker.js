/**
 * Wrapper around Pikaday
 */
let TEMPLATE = `
<input ref="datepicker" type="text">
`;

Vue.component('attract-date-picker', {
    props: {
        value: String
    },
    template: TEMPLATE,
    data() {
        return {
            picker: null // inited in this.initDatePicker()
        }
    },
    mounted: function () {
        this.$nextTick(this.initDatePicker);
    },
    watch: {
        value(newValue, oldValue) {
            this.picker.setDate(newValue);
        }
    },
    methods: {
        initDatePicker() {
            let vm = this;
            this.picker = new Pikaday(
                {
                    field: this.$refs.datepicker,
                    firstDay: 1,
                    showTime: false,
                    use24hour: true,
                    format: 'dddd D, MMMM YYYY',
                    disableWeekends: true,
                    timeLabel: 'Time: ',
                    autoClose: true,
                    incrementMinuteBy: 15,
                    yearRange: [new Date().getFullYear(),new Date().getFullYear() + 5],
                    onSelect: function(date) {
                        // This is a bit ugly. Can we solve this in a better way?
                        let dateAsConfigedInEve = this.getMoment().format('ddd, DD MMM YYYY [00:00:00 GMT]')
                        vm.$emit('input', dateAsConfigedInEve);
                    }
                });
            this.picker.setDate(this.value);
        }
    },
  }
);
