/**
 * MultiEditEngine
 *      n-MultiProperty
 *          1-PropertyCB
 * 
 * Class to edit multiple objects at the same time.
 * It keeps track of what object properties has been edited, which properties that still diffs,
 * 
 * @example
 * let myObjects = [{
 *          'name': 'Bob',
 *          'personal': {
 *              'hobby': 'Fishing'
 *          }
 *      },{
 *          'name': 'Greg',
 *          'personal': {
 *              'hobby': 'Movies'
 *          }
 *      }]
 * // Create engine with list of objects to edit, and the properties we want to be able to edit.
 * let engine = new MultiEditEngine(myObjects,'name', 'personal.hobby');
 * 
 * engine.getProperty('personal.hobby').isConclusive();  // false since one is 'Fishing' and one 'Movies'
 * engine.getProperty('personal.hobby').isEdited();      // false
 * 
 * engine.getProperty('personal.hobby').value = 'Fishing';
 * engine.getProperty('personal.hobby').isConclusive();  // true
 * engine.getProperty('personal.hobby').isEdited();      // true
 * engine.getProperty('personal.hobby').getOriginalValues(); // A set with the original values 'Fishing' and 'Movies'
 * 
 * engine.getProperty('name').isConclusive();   // false since one is 'Bob' and one is 'Greg'
 * engine.getProperty('personal.hobby').isEdited();      // false since this property has not been edited
 * 
 * let updatedObjects = engine.createUpdatedItems();
 * // updatedObjects is now: [{'name': 'Greg', 'hobby': 'Fishing'}]
 * // myObjects is still unchanged.
 */

function areEqual(valA, valB) {
    if(Array.isArray(valB) && Array.isArray(valB)) {
        if(valA.length === valB.length) {
            for (let i = 0; i < valA.length; i++) {
                if(!areEqual(valA[i], valB[i])) return false;
            }
            return true;
        }
        return false;
    } else {
        return valA === valB;
    }
}

class UniqueValues {
    constructor() {
        this._values = new Set();
    }

    get size() {
        return this._values.size;
    }

    /**
     * 
     * @param {*} valueCandidate 
     */
    addIfUnique(valueCandidate) {
        if (Array.isArray(valueCandidate)) {
            for (const uniqueValue of this._values) {
                if(!Array.isArray(uniqueValue)) continue;
                if(areEqual(valueCandidate, uniqueValue)) {
                    // not a new value. Don't add
                    return;
                }
            }
            this._values.add(valueCandidate);
        } else {
            this._values.add(valueCandidate);
        }
    }

    getValueOrInconclusive() {
        if (this.size === 1) {
            return this._values.values().next().value;
        }
        return INCONCLUSIVE;
    }

    getValues() {
    return new Set([...this._values]);
    }

    _areArraysEqual(arrA, arrB) {
        if(arrA.size === arrB.size) {
            for (let i = 0; i < arrA.length; i++) {
                if(arrA[i] !== arrB[i]) return false;
            }
            return true;
        }
        return false;
    }
 }

 class PropertyCB {
     constructor(propertyPath) {
         this.name = propertyPath;
         this._propertyPath = propertyPath.split('.');
         this._propertyKey = this._propertyPath.pop();
     }

     /**
     * Get the property from the item
     * @param {Object} item 
     * @returns {*} Property value
     */
     getValue(item) {
         let tmp = item;
         for (const key of this._propertyPath) {
            tmp = (tmp || {})[key]
         }
         return (tmp || {})[this._propertyKey];
     }

     /**
     * Assign a new value to the property
     * @param {Object} item 
     * @param {*} newValue 
     */
     setValue(item, newValue) {
        let tmp = item;
        for (const key of this._propertyPath) {
            tmp[key] = tmp[key] || {};
            tmp = tmp[key];
        }
        tmp[this._propertyKey] = newValue;
    }
 }

// Dummy object to indicate that a property is unedited.
const NOT_SET = Symbol('Not Set');
const INCONCLUSIVE = Symbol('Inconclusive');

class MultiProperty {
    /**
     * 
     * @param {String} propPath Dot separeted path to property 
     */
    constructor(propPath) {
        this.propCB = new PropertyCB(propPath);;
        this.originalValues = new UniqueValues();
        this.newValue = NOT_SET;
    }

    get value() {
        return this.newValue !== NOT_SET ?
            this.newValue :
            this._getOriginalValue();
    }

    set value(newValue) {
        if (areEqual(newValue, this._getOriginalValue())) {
            this.reset();
        } else {
            this.newValue = newValue;
        }
    }
    
    /**
     * Returns a Set with all values the different object has for the property.
     * @returns {Set}
     */
    getOriginalValues() {
        return this.originalValues.getValues();
    }
    
    /**
     * Ture if property has been edited.
     * @returns {Boolean}
     */
    isEdited() {
        return this.newValue !== NOT_SET;
    }
    
    /**
     * Undo changes to property.
     */
    reset() {
        this.newValue = NOT_SET;
    }
    
    /**
     * True if all objects has the same value for this property.
     * @returns {Boolean}
     */
    isConclusive() {
        if (this.newValue !== NOT_SET) {
            return true;
        }
        return this.originalValues.size == 1;
    }

    _applyNewValue(item) {
        if (this.isEdited()) {
            this.propCB.setValue(item, this.newValue);
            return true;
        }
        return false;
    }

    _getOriginalValue() {
        let origVal = this.originalValues.getValueOrInconclusive()
        return origVal !== INCONCLUSIVE ?
                origVal : undefined;
    }
    
    _addValueFrom(item) {
        this.originalValues.addIfUnique(this.propCB.getValue(item));
    }
}

class MultiEditEngine {
    /**
     * @param {Array<Object>} items An array with the objects to be edited.
     * @param  {...String} propertyPaths Dot separeted paths to properties. 'name', 'properties.status'
     */
    constructor(items, ...propertyPaths) {
        this.originalItems = items;
        this.properties = this._createMultiproperties(propertyPaths);
    }

    /**
     * 
     * @param {String} propName 
     * @returns {MultiProperty}
     */
    getProperty(propName) {
        return this.properties[propName];
    }

    /**
     * True if all the objects has the same value for all of its monitored properties.
     * @returns {Boolean}
     */
    isConclusive() {
        for (const key in this.properties) {
            const prop = this.properties[key];
            if (prop.isConclusive()) {
                return true;
            }
        }
        return false;
    }

    /**
     * True if at least one property has been edited.
     * @returns {Boolean}
     */
    isEdited() {
        for (const key in this.properties) {
            const prop = this.properties[key];
            if (prop.isEdited()) {
                return true;
            }
        }
        return false;
    }

    /**
     * Returns an array with copies of the objects with there new values.
     * Only the updated objects are included in the array.
     * @returns {Array<Object>}
     */
    createUpdatedItems() {
        let updatedItems = [];
        for (const it of this.originalItems) {
            let itemCopy = JSON.parse(JSON.stringify(it));
            let hasChanged = false;
            for (const key in this.properties) {
                const prop = this.properties[key];
                hasChanged |= prop._applyNewValue(itemCopy);
            }
            if(hasChanged) {
                updatedItems.push(itemCopy);
            }
        }
        return updatedItems;
    }

    /**
     * True if all items has 'PUT' in 'allowed_methods'. If object has now 'allowed_methods' we return true
     * @returns {Boolean}
     */
    allowedToEdit() {
        for (const it of this.originalItems) {
            if(!it.allowed_methods) continue;
            if(!it.allowed_methods.includes('PUT')) {
                return false;
            }
        }
        return true;
    }

    /**
     * Undo all edits on all properties.
     */
    reset() {
        for (const key in this.properties) {
            this.properties[key].reset();
        }
    }

    _createMultiproperties(propertyPaths) {
        let retval = {}
        for (const propPath of propertyPaths) {
            let prop = new MultiProperty(propPath);
            this.originalItems.forEach(prop._addValueFrom.bind(prop));
            retval[propPath] = prop;
        }
        return retval;
    }
}

export { MultiEditEngine }
