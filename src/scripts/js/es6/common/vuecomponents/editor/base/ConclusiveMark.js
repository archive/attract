
const TEMPLATE =`
<i 
    :class="classes"
    :title="toolTip"
/>
`;

/**
 * Draws a chain icon. If property is inconclusive it becomes a broken chain.
 */
Vue.component('attract-property-conslusive-mark', {
    template: TEMPLATE,
    props: {
        prop: Object, // MultiProperty
    },
    computed: {
        classes() {
            return this.prop.isConclusive() ? 'pi-link' : 'pi-unlink';
        },
        toolTip() {
            if (this.prop.isConclusive()) {
                return 'All objects has the same value'
            } else {
                let values = this.prop.getOriginalValues();
                let toolTip = 'Objects has diverging values:';
                let i = 0;
                for (const it of values) {
                    if (i === 5) {
                        toolTip += `\n...`;
                        break;
                    }
                    toolTip += `\n${++i}: ${this.shorten(it)}`;
                }
                return toolTip;
            }
        },
    },
    methods: {
        shorten(value) {
            let s = `${value}`;
            return s.length < 30 ? s : `${s.substr(0, 27)}...`
        }
    },
});
