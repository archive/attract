import {MultiEditEngine} from './MultiEditEngine'
let UnitOfWorkTracker = pillar.vuecomponents.mixins.UnitOfWorkTracker;


/**
 * Properties to be edited and/or read using the multi editor engine.
 */
const BaseProps = Object.freeze({
    NAME: 'name',
    DESCRIPTION: 'description',
    STATUS: 'properties.status',
    NODE_TYPE: 'node_type',
});

let ALL_BASE_PROPERTIES = [];

for (const key in BaseProps) {
    ALL_BASE_PROPERTIES.push(BaseProps[key]);
}


/**
 * The base implementation of node editor.
 * Extend to fit your needs.
 * @emits objects-are-edited(isEdited) When the user starts editing the objects.
 */
let EditorBase = Vue.component('attract-editor-Base', {
    mixins: [UnitOfWorkTracker],
    props: {
        items: Array, // Array of objects to be edited.
        project: Object,
    },
    data() {
        return {
            multiEditEngine: this.createEditorEngine(ALL_BASE_PROPERTIES),
            isSaving: false,
        }
    },
    watch: {
        items() {
            this.multiEditEngine = this.createEditorEngine(ALL_BASE_PROPERTIES); // MultiEditEngine
        },
        statusPropEdited(isEdited) {
            if(isEdited && this.items.length === 1) {
                // Auto save on status is convenient, but could lead to head ache in multi edit.
                this.save();
            }
        },
        isEdited(isEdited) {
            this.$emit('objects-are-edited', isEdited);
        }
    },
    computed: {
        isMultpleItems() {
            return this.items.length > 1;
        },
        nodeTypeProp() {
            return this.multiEditEngine.getProperty(BaseProps.NODE_TYPE);
        },
        allowedStatuses() {
            let tmp = this.project.node_types.filter((it) => it.name === this.nodeTypeProp.value);
            if(tmp.length === 1) {
                let nodeTypeDefinition = tmp[0];
                return nodeTypeDefinition.dyn_schema.status.allowed;
            }
            console.log('Failed to find allowed statused for node type:', this.nodeTypeProp.value);
            return [];
        },
        allowedStatusesPair() {
            function format(status) {
                // in_progress => In progress
                let first = status[0].toUpperCase();
                let last = status.substr(1).replace('_', ' ');
                return `${first}${last}`;
            }
            return this.allowedStatuses.map(it => {
                return {
                    id: it,
                    text: format(it)
                }
            });
        },
        nameProp() {
            return this.multiEditEngine.getProperty(BaseProps.NAME);
        },
        descriptionProp() {
            return this.multiEditEngine.getProperty(BaseProps.DESCRIPTION);
        },
        statusProp() {
            return this.multiEditEngine.getProperty(BaseProps.STATUS);
        },
        statusPropEdited() {
            return this.statusProp.isEdited();
        },
        editorClasses() {
            let status = this.statusProp.isConclusive() ? this.statusProp.value : 'inconclusive';
            let classes = {}
            classes[`status-${status}`] = true;
            return classes;
        },
        isEdited() {
            return this.multiEditEngine.isEdited();
        },
        canEdit() {
            let canUseAttract = attract.auth.AttractAuth.canUserCanUseAttract(ProjectUtils.projectId());
            return canUseAttract && this.multiEditEngine.allowedToEdit();
        }
    },
    methods: {
        /**
         * @param {MultiProperty} prop 
         * @returns {Object} Css classes for property
         */
        classesForProperty(prop) {
            return {
                'inconclusive': !prop.isConclusive(),
                'edited': prop.isEdited(),
            }
        },
        createEditorEngine(props) {
            return new MultiEditEngine(this.items, ...props);
        },
        save() {
            let toBeSaved = this.multiEditEngine.createUpdatedItems();
            let promises = toBeSaved.map(pillar.api.thenUpdateNode);
            this.isSaving = true;
            this.unitOfWork(
                Promise.all(promises)
                    .then(() => {
                        this.$emit('saved-items');
                    })
                    .catch((err) => {toastr.error(pillar.utils.messageFromError(err), 'Save Failed')})
                    .finally(() => this.isSaving = false)
            );
        },
    },
});

export {EditorBase, BaseProps}

