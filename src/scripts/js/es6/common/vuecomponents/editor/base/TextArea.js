const TEMPLATE = `
<textarea ref="inputField"
    v-bind:value="value"
    v-on:input="$emit('input', $event.target.value)"
    class="input-transparent"
    type="text"
    rows="2"/>
`;
/**
 * Wrapper around regular textarea to make it grow in length as you type.
 */
Vue.component('attract-editor-text-area', {
    template: TEMPLATE,
    props: {
        value: String,
    },
    watch:{
        value() {
            this.$nextTick(this.autoSizeInputField);
        }
    },
    mounted() {
        this.$nextTick(this.autoSizeInputField);
    },
    methods: {
        autoSizeInputField() {
            let elInputField = this.$refs.inputField;
            elInputField.style.cssText = 'height:auto; padding:0';
            let newInputHeight = elInputField.scrollHeight + 20;
            elInputField.style.cssText = `height:${ newInputHeight }px`;
        }
    },
});

