import './base/TextArea'
import './base/ConclusiveMark'
import './base/Select2'
import './base/DatePicker'
import {EditorBase, BaseProps} from './base/EditorBase'

const TEMPLATE =`
<div class="attract-box task with-status"
    :class="editorClasses"
>
    <div class="input-group">
    <attract-property-conslusive-mark
        v-if="isMultpleItems"
        :prop="nameProp"
    />
    <input class="item-name" name="name" type="text" placeholder="Task Title"
        :disabled="!canEdit"
        :class="classesForProperty(nameProp)"
        v-model="nameProp.value"/>
        <div class="dropdown" style="margin-left: auto"
            v-if="canEdit"
        >
            <button class="btn btn-outline-success dropdown-toggle" id="item-dropdown" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <i class="pi-more-vertical"/>
            </button>
            <ul class="dropdown-menu" aria-labelledby="item-dropdown">
                <li class="copy-to-clipboard" 
                    v-if="!isMultpleItems"
                    :data-clipboard-text="items[0]._id">
                    <a href="javascript:void(0)">
                        <i class="pi-clipboard-copy"/>
                        Copy ID to Clipboard
                    </a>
                </li>
                <li class="copy-to-clipboard"
                    v-if="!isMultpleItems"
                    :data-clipboard-text="'[' + items[0].properties.shortcode + ']'">
                    <a href="javascript:void(0)">
                        <i class="pi-clipboard-copy"/>
                        Copy Shortcode for SVN Commits to Clipboard
                    </a>
                </li>
                <li class="divider" role="separator"/>
                <li class="item-delete">
                    <a href="javascript:void(0)"
                        @click="deleteTasks"
                    >
                        <i class="pi-trash"/>
                        {{ isMultpleItems ? "Delete Tasks" : "Delete Task" }}
                    </a>
                </li>
            </ul>
        </div>
    </div>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="descriptionProp"
        />
        <attract-editor-text-area
            placeholder="Description"
            :disabled="!canEdit"
            :class="classesForProperty(descriptionProp)"
            v-model="descriptionProp.value"
        />
    </div>
    <div class="input-group-flex">
        <div class="input-group field-type"
            v-if="(canChangeTaskType)"
        >
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="taskTypeProp"
            />
            <label id="task-task_type">
                Type:
            </label>
            <select name="task_type" aria-describedby="task-task_type"
                :class="classesForProperty(taskTypeProp)"
                :disabled="!canEdit"
                v-model="taskTypeProp.value"
            >
            <option value=undefined disabled="true" v-if="!statusProp.isConclusive()"> *** </option>
            <option v-for="it in allowedTaskTypesPair"
                :key="it.id"
                :value="it.id"
            >{{it.text}}</option>
            </select></div>
        <div class="input-group">
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="statusProp"
            />
            <label for="item-status">Status:</label>
            <select class="input-transparent" id="item-status" name="status"
                :class="classesForProperty(statusProp)"
                :disabled="!canEdit"
                v-model="statusProp.value"
                >
                <option value=undefined disabled="true" v-if="!statusProp.isConclusive()"> *** </option>
                <option v-for="it in allowedStatusesPair"
                    :key="it.id"
                    :value="it.id"
                >{{it.text}}</option>
            </select>
        </div>
    </div>
    <div class="input-group-separator"/>
    <div class="input-group select_multiple">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="assignedToProp"
        />
        <label>
            Assignees:
        </label>
        <attract-select2
            :class="classesForProperty(assignedToProp)"
            :options="users"
            :disabled="!canEdit"
            v-model="assignedToProp.value">
            <option value=undefined disabled="true" v-if="!assignedToProp.isConclusive()"> *** </option>
        </attract-select2>
    </div>
    <div class="input-group">
        <attract-property-conslusive-mark
            v-if="isMultpleItems"
            :prop="dueDateProp"
        />
        <label>
            Due Date:
        </label>
        <attract-date-picker id="item-due_date" name="due_date" placeholder="Deadline for Task"
            :class="classesForProperty(dueDateProp)"
            :disabled="!canEdit"
            v-model="dueDateProp.value"
        />
    </div>
    <div class="input-group-separator"/>
    <div class="input-group"
        v-if="canEdit"
    >
        <button class="btn btn-outline-success btn-block" id="item-save" type="submit"
            @click="save"
            :disabled="isSaving"
        >
            <i class="pi-check"/>
            Save Task
        </button>
    </div>
</div>
`;


const AllProps = Object.freeze({
    ...BaseProps,
    PARENT: 'parent',
    TASK_TYPE: 'properties.task_type',
    DUE_DATE: 'properties.due_date',
    ASSIGNED_TO: 'properties.assigned_to.users',
    SHORT_CODE: 'properties.short_code',
});

let ALL_PROPERTIES = [];

for (const key in AllProps) {
    ALL_PROPERTIES.push(AllProps[key]);
}

let TaskEditor = Vue.component('attract-editor-task', {
    template: TEMPLATE,
    extends: EditorBase,
    props: {
        contextType: String
    },
    data() {
        return {
            multiEditEngine: this.createEditorEngine(ALL_PROPERTIES),
            users: [],
        }
    },
    created() {
        this.fetchUsers()
    },
    watch: {
        items() {
            this.multiEditEngine = this.createEditorEngine(ALL_PROPERTIES);
        },
    },
    computed: {
        parentProp() {
            return this.multiEditEngine.getProperty(AllProps.PARENT);
        },
        taskTypeProp() {
            return this.multiEditEngine.getProperty(AllProps.TASK_TYPE);
        },
        allowedTaskTypes() {
            let shot_task_types = this.project.extension_props.attract.task_types.attract_shot;

            return ['generic', ...shot_task_types];
        },
        canChangeTaskType() {
            return this.parentProp.isConclusive() && !this.parentProp.value;
        },
        allowedTaskTypesPair() {
            function format(status) {
                // hair_sim => Hair sim
                let first = status[0].toUpperCase();
                let last = status.substr(1).replace('_', ' ');
                return `${first}${last}`;
            }

            return this.allowedTaskTypes.map(it => {
                return {
                    id: it,
                    text: format(it)
                }
            });
        },
        dueDateProp() {
            return this.multiEditEngine.getProperty(AllProps.DUE_DATE);
        },
        assignedToProp() {
            return this.multiEditEngine.getProperty(AllProps.ASSIGNED_TO);
        },
    },
    methods: {
        fetchUsers() {
            pillar.api.thenGetProjectUsers(this.project._id)
                .then(users => {
                    this.users = users._items.map(it =>{
                        return {
                            id: it._id,
                            text: it.full_name,
                        };
                    });
                });
        },
        deleteTasks() {
            this.items.map(pillar.api.thenDeleteNode);
        }
    },
});

export {TaskEditor}
