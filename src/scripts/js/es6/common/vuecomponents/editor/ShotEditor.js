import './base/TextArea'
import './base/ConclusiveMark'
import {EditorBase, BaseProps} from './base/EditorBase'

const TEMPLATE =`
<div>
    <div class="attract-box shot with-status" 
        :class="editorClasses"
    >
        <div class="input-group">
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="nameProp"
            />
            <span title="Shot names can only be updated from Blender." class="item-name">
                {{ valueOrNA(nameProp.value) }}
            </span>
            <button class="copy-to-clipboard btn item-id" name="Copy to Clipboard" type="button" title="Copy ID to clipboard"
                v-if="!isMultpleItems"
                :data-clipboard-text="items[0]._id"
            >
                ID
            </button>
        </div>
        <div class="input-group">
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="descriptionProp"
            />
            <attract-editor-text-area
                placeholder="Description"
                :disabled="!canEdit"
                :class="classesForProperty(descriptionProp)"
                v-model="descriptionProp.value"
            />
        </div>
        <div class="input-group">
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="statusProp"
            />
            <label for="item-status">
                Status:
            </label>
            <select id="item-status" name="status" class="input-transparent"
                :class="classesForProperty(statusProp)"
                :disabled="!canEdit"
                v-model="statusProp.value"
                >
                <option value=undefined disabled="true" v-if="!statusProp.isConclusive()"> *** </option>
                <option v-for="it in allowedStatusesPair"
                    :key="it.id"
                    :value="it.id"
                >{{it.text}}</option>
            </select>
        </div>
        <div class="input-group">
            <attract-property-conslusive-mark
                v-if="isMultpleItems"
                :prop="notesProp"
            />
            <attract-editor-text-area
                placeholder="Notes"
                :class="classesForProperty(notesProp)"
                :disabled="!canEdit"
                v-model="notesProp.value"
            />
        </div>
        <div class="input-group-separator"/>
        <div class="input-group"
            v-if="canEdit"
        >
            <button class="btn btn-outline-success btn-block" id="item-save" type="submit"
                @click="save"
                :disabled="isSaving"
            >
                <i class="pi-check"/>Save Shot
            </button>
        </div>
    </div>


    <div class="attract-box">
        <div class="table item-properties">
            <div class="table-body">
                <div class="table-row">
                    <div class="table-cell">
                    <attract-property-conslusive-mark
                        v-if="isMultpleItems"
                        :prop="updatedProp"
                    />
                        Last Update
                    </div>
                    <div :title="updatedProp.value" class="table-cell">
                        <span role="button" data-toggle="collapse" data-target="#task-time-creation" aria-expanded="false" aria-controls="#task-time-creation">
                            {{ prettyDate(updatedProp.value) }}
                        </span>
                        <div id="task-time-creation" class="collapse">
                            {{ prettyDate(createdProp.value) }}
                        </div>
                    </div>
                </div>
                <div class="table-row">
                    <div class="table-cell">
                        <attract-property-conslusive-mark
                            v-if="isMultpleItems"
                            :prop="usedInEditProp"
                        />
                        Used in Edit
                    </div>
                    <div title="Whether this shot is used in the edit." class="table-cell text-capitalize">
                        {{ formatBool(usedInEditProp.value) }}
                    </div>
                </div>
                <div class="table-row">
                    <div class="table-cell">
                        <attract-property-conslusive-mark
                            v-if="isMultpleItems"
                            :prop="cutInTimelineProp"
                        />
                        Cut-in
                    </div>
                    <div title="Frame number of the first visible frame of this shot." class="table-cell">
                        at frame {{ valueOrNA(cutInTimelineProp.value) }}
                    </div>
                </div>
                <div class="table-row">
                    <div class="table-cell">
                        <attract-property-conslusive-mark
                            v-if="isMultpleItems"
                            :prop="trimStartProp"
                        />
                        Trim Start
                    </div>
                    <div title="How many frames were trimmed off the start of the shot in the edit." class="table-cell">
                        {{ valueOrNA(trimStartProp.value) }} frames
                    </div>
                </div>
                <div class="table-row">
                    <div class="table-cell">
                        <attract-property-conslusive-mark
                            v-if="isMultpleItems"
                            :prop="trimEndProp"
                        />
                        Trim End
                    </div>
                    <div title="How many frames were trimmed off the end of the shot in the edit." class="table-cell">
                        {{ valueOrNA(trimEndProp.value) }} frames
                    </div>
                </div>
                <div class="table-row">
                    <div class="table-cell">
                        <attract-property-conslusive-mark
                            v-if="isMultpleItems"
                            :prop="durationInEditProp"
                        />
                        Duration in Edit
                    </div>
                    <div title="Duration of the visible part of this shot." class="table-cell">
                        {{ valueOrNA(durationInEditProp.value) }} frames
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
`;


const AllProps = Object.freeze({
    ...BaseProps,
    UPDATED: '_updated',
    CREATED: '_created',
    NOTES: 'properties.notes',
    USED_IN_EDIT: 'properties.used_in_edit',
    CUT_IN_TIMELINE: 'properties.cut_in_timeline_in_frames',
    TRIM_START: 'properties.trim_start_in_frames',
    TRIM_END: 'properties.trim_end_in_frames',
    DURATION_IN_EDIT: 'properties.duration_in_edit_in_frames',
});

let ALL_PROPERTIES = [];

for (const key in AllProps) {
    ALL_PROPERTIES.push(AllProps[key]);
}

let ShotEditor = Vue.component('attract-editor-shot', {
    template: TEMPLATE,
    extends: EditorBase,
    data() {
        return {
            multiEditEngine: this.createEditorEngine(ALL_PROPERTIES),
        }
    },
    watch: {
        items() {
            this.multiEditEngine = this.createEditorEngine(ALL_PROPERTIES);
        },
    },
    computed: {
        notesProp() {
            return this.multiEditEngine.getProperty(AllProps.NOTES);
        },
        updatedProp() {
            return this.multiEditEngine.getProperty(AllProps.UPDATED);
        },
        createdProp() {
            return this.multiEditEngine.getProperty(AllProps.CREATED);
        },
        usedInEditProp() {
            return this.multiEditEngine.getProperty(AllProps.USED_IN_EDIT);
        },
        cutInTimelineProp() {
            return this.multiEditEngine.getProperty(AllProps.CUT_IN_TIMELINE);
        },
        trimStartProp() {
            return this.multiEditEngine.getProperty(AllProps.TRIM_START);
        },
        trimEndProp() {
            return this.multiEditEngine.getProperty(AllProps.TRIM_END);
        },
        durationInEditProp() {
            return this.multiEditEngine.getProperty(AllProps.DURATION_IN_EDIT);
        },
    },
    methods: {
        valueOrNA(value) {
            return value ? value : 'N/A'
        },
        formatBool(value) {
            switch (value) {
                case true: return 'Yes';
                case false: return 'No';
                default: return 'N/A';
            }
        },
        prettyDate(value) {
            if(value) {
                return pillar.utils.prettyDate(value, true);
            }
            return 'N/A';
        }
    },
});

export {ShotEditor}
