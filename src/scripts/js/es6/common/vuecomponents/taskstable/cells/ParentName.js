let CellDefault = pillar.vuecomponents.table.cells.renderer.CellDefault;

const TEMPLATE =`
<div>
    <a
        v-if="rawCellValue"
        @click="onClick"
        :href="cellLink"
    >
        {{ cellValue }}
    </a>
</div>
`;

let ParentNameCell = Vue.component('pillar-cell-parent-name', {
    extends: CellDefault,
    template: TEMPLATE,
    computed: {
        cellTitle() {
            return this.rawCellValue;
        },
        cellLink() {
            let project_url = ProjectUtils.projectUrl();
            let item_type = this.itemType();
            return `/attract/${project_url}/${item_type}s/${this.rowObject.getParent()._id}`;
        },
        embededLink() {
            return this.cellLink;
        }
    },
    methods: {
        onClick(event) {
            event.preventDefault(); // Don't follow link, but let event bubble and the row will handle it
        },
        itemType() {
            let node_type = this.rowObject.getParent().node_type;
            return node_type.replace('attract_', ''); // eg. attract_task to task
        }
    },
});

export { ParentNameCell }
