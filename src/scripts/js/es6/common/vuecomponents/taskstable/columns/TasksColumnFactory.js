import { Status } from '../../attracttable/columns/Status'
import { RowObject } from '../../attracttable/columns/RowObject'
import { TaskDueDate } from '../../attracttable/columns/TaskDueDate'
import { TaskType } from './TaskType'
import { ShortCode } from './ShortCode'
import { ParentName } from './ParentName'

let ColumnFactoryBase = pillar.vuecomponents.table.columns.ColumnFactoryBase;
let Created = pillar.vuecomponents.table.columns.Created;
let Updated = pillar.vuecomponents.table.columns.Updated;


class TasksColumnFactory extends ColumnFactoryBase{
    thenGetColumns() {
        return Promise.resolve([
            new Status(),
            new ParentName(),
            new RowObject(),
            new ShortCode(),
            new TaskType(),
            new TaskDueDate(),
            new Created(),
            new Updated(),
        ]);
    }
}

export { TasksColumnFactory }
