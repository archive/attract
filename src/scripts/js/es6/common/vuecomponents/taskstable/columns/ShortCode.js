let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;

class ShortCode extends ColumnBase {
    constructor() {
        super('Short Code', 'short-code');
    }

    getRawCellValue(rowObject) {
        return rowObject.getTask().properties.shortcode || '';
    }
}

export { ShortCode }
