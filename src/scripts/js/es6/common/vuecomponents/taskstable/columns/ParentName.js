let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;
import {ParentNameCell} from '../cells/ParentName'

class ParentName extends ColumnBase {
    constructor() {
        super('Parent', 'parent-name');
    }

    getCellRenderer(rowObject) {
        return ParentNameCell.options.name;
    }

    getRawCellValue(rowObject) {
        if(!rowObject.getParent()) return '';
        return rowObject.getParent().name || '<No Name>';
    }

    compareRows(rowObject1, rowObject2) {
        let parent1 = rowObject1.getParent();
        let parent2 = rowObject2.getParent();
        if (parent1 && parent2) {
            if (parent1.name === parent2.name) {
                return parent1._id < parent2._id ? -1 : 1;
            }
        }
        return super.compareRows(rowObject1, rowObject2);
    }
}

export { ParentName }
