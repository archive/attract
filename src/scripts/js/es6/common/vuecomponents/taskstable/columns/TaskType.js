let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;

class TaskType extends ColumnBase {
    constructor() {
        super('Type', 'task-type');
    }

    getRawCellValue(rowObject) {
        return rowObject.getTask().properties.task_type || '';
    }
}

export { TaskType }
