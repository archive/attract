let PillarTable = pillar.vuecomponents.table.PillarTable;
import {TasksColumnFactory} from './columns/TasksColumnFactory'
import {TaskRowsSource} from './rows/TaskRowsSource'
import {RowFilter} from '../attracttable/rows/filter/RowFilter'

const TEMPLATE =`
<div class="pillar-table-actions">
    <button class="action"
        v-if="canAddTask"
        @click="createNewTask"
    >
        <i class="pi-plus">New Task</i>
    </button>
</div>
`;

let TableActions = {
    template: TEMPLATE,
    computed: {
        canAddTask() {
            let projectId = ProjectUtils.projectId();
            return attract.auth.AttractAuth.canUserCreateTask(projectId);
        }
    },
    methods: {
        createNewTask(event) {
            thenCreateTask(undefined, 'generic')
            .then((task) => {
                this.$emit('item-clicked', event, task._id);
            });
        }
    },
}

let TasksTable = Vue.component('attract-tasks-table', {
    extends: PillarTable,
    props: {
        project: Object
    },
    data() {
        return {
            columnFactory: new TasksColumnFactory(this.project),
            rowsSource: new TaskRowsSource(this.project._id),
            rowFilterConfig: {validStatuses: this.getValidStatuses()}
        }
    },
    methods: {
        getValidStatuses() {
            for (const it of this.project.node_types) {
                if(it.name === 'attract_task'){
                    return it.dyn_schema.status.allowed;
                }
            }
            console.warn('Did not find allowed statuses for node type attract_task');
            return [];
        }
    },
    components: {
        'pillar-table-actions': TableActions,
        'pillar-table-row-filter': RowFilter,
    }
});

export {TasksTable}
