import { AttractRowsSourceBase } from '../../attracttable/rows/AttractRowsSourceBase'
import { TaskRow } from './TaskRow'

class TaskRowsSource extends AttractRowsSourceBase {
    constructor(projectId) {
        super(projectId, 'attract_task', TaskRow);
    }

    thenGetRowObjects() {
        return attract.api.thenGetProjectTasks(this.projectId)
            .then((result) => {
                let tasks = result._items;
                this.initRowObjects(tasks);
            });
    }
}

export { TaskRowsSource }
