import {AttractRowBase} from '../../attracttable/rows/AttractRowBase'

class TaskRow extends AttractRowBase {
    constructor(task) {
        super(task);
        this.parent = undefined;
        if (task.parent && task.parent._id) {
            // Deattach parent from task to avoid parent to be overwritten when task is updated
            let parentId = task.parent._id;
            this.parent = task.parent;
            task.parent = parentId;
            pillar.events.Nodes.onUpdated(parentId, this.onParentUpdated.bind(this));
        }
    }

    getTask() {
        return this.underlyingObject;
    }

    getParent() {
        return this.parent;
    }

    onParentUpdated(event) {
        this.parent = event.detail;
    }
}

export { TaskRow }
