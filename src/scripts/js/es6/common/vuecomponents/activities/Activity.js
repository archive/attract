const TEMPLATE =`
<li>
    <img class="actor-avatar"
        :src="activity.actor_user.avatar"
    />
    <span class="date"
        :title="activity._created">
        {{ prettyCreated }}
    </span>
    <span class="actor">
        {{ activity.actor_user.full_name }}
    </span>
    <span class="verb">
        {{ activity.verb }}
    </span>
</li>
`;

Vue.component('attract-activity', {
    template: TEMPLATE,
    props: {
        activity: Object,
    },
    computed: {
        prettyCreated() {
            return pillar.utils.prettyDate(this.activity._created, true);
        }
    },
});
