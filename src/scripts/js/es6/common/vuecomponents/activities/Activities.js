import './Activity'

const TEMPLATE =`
<div class="d-activity">
    <ul>
        <attract-activity
            v-for="a in activities"
            :key="a._id"
            :activity="a"
        />
    </ul>
</div>
`;

Vue.component('attract-activities', {
    template: TEMPLATE,
    props: {
        objectId: String,
        outdated: {
            type: Boolean,
            default: true
        }
    },
    data() {
        return {
            activities: [],
        }
    },
    watch: {
        objectId() {
            this.fetchActivities();
        },
        outdated(isOutDated) {
            if(isOutDated) {
                this.fetchActivities();
            }
        }
    },
    created() {
        this.fetchActivities()
    },
    methods: {
        fetchActivities() {
            pillar.api.thenGetNodeActivities(this.objectId)
            .then(it => {
                this.activities = it['_items'];
                this.$emit('activities-updated');
            });
        }
    },
});
