import {CellPicture} from '../cells/renderer/Picture'
let ColumnBase = pillar.vuecomponents.table.columns.ColumnBase;

export class Picture extends ColumnBase {
    constructor() {
        super('Thumbnail', 'thumbnail');
        this.isSortable = false;
    }
    getCellRenderer(rowObject) {
        return CellPicture.options.name;
    }
    getRawCellValue(rowObject) {
        return rowObject.underlyingObject.picture;
    }
}
