import { TaskColumn } from '../../attracttable/columns/Tasks';
import { FirstTaskDueDate, NextTaskDueDate, LastTaskDueDate } from '../../attracttable/columns/TaskDueDate';
import { Status } from '../../attracttable/columns/Status';
import { Picture } from '../columns/Picture'
import { RowObject } from '../../attracttable/columns/RowObject'
let ColumnFactoryBase = pillar.vuecomponents.table.columns.ColumnFactoryBase;
let Created = pillar.vuecomponents.table.columns.Created;
let Updated = pillar.vuecomponents.table.columns.Updated;


class ShotsColumnFactory extends ColumnFactoryBase{
    constructor(project) {
        super();
        this.project = project;
    }

    thenGetColumns() {
            let taskTypes = this.project.extension_props.attract.task_types.attract_shot;
            let taskColumns = taskTypes.map((tType) => {
                return new TaskColumn(tType, 'shot-task');
            })

            return Promise.resolve(
                [new Status(), new Picture(), new RowObject()]
                .concat(taskColumns)
                .concat([new NextTaskDueDate(), new Created(), new Updated()])
                );
    }
}

export { ShotsColumnFactory }
