let PillarTable = pillar.vuecomponents.table.PillarTable;
import {ShotsColumnFactory} from './columns/ShotsColumnFactory'
import {ShotRowsSource} from './rows/ShotRowsSource'
import {RowFilter} from '../attracttable/rows/filter/RowFilter'

let ShotsTable = Vue.component('attract-shots-table', {
    extends: PillarTable,
    props: {
        project: Object
    },
    data() {
        return {
            columnFactory: new ShotsColumnFactory(this.project),
            rowsSource: new ShotRowsSource(this.project._id),
            rowFilterConfig: {validStatuses: this.getValidStatuses()}
        }
    },
    methods: {
        getValidStatuses() {
            for (const it of this.project.node_types) {
                if(it.name === 'attract_shot'){
                    return it.dyn_schema.status.allowed;
                }
            }
            console.warn('Did not find allowed statuses for node type attract_shot');
            return [];
        }
    },
    components: {
        'pillar-table-row-filter': RowFilter,
    },
});

export { ShotsTable }
