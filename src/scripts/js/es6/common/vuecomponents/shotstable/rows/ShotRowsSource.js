import { AttractRowsSourceBase } from '../../attracttable/rows/AttractRowsSourceBase'
import { ShotRow } from './ShotRow'

class ShotRowsSource extends AttractRowsSourceBase {
    constructor(projectId) {
        super(projectId, 'attract_asset', ShotRow);
    }
    
    thenGetRowObjects() {
        return attract.api.thenGetProjectShots(this.projectId)
            .then((result) => {
                let shots = result._items;
                this.initRowObjects(shots);
            });
    }
}

export { ShotRowsSource }
