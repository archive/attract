import {AttractRowBase} from '../../attracttable/rows/AttractRowBase'
import { TaskEventListener } from '../../attracttable/rows/TaskEventListener';
import { TaskRow } from '../../taskstable/rows/TaskRow';

class ShotRow extends AttractRowBase {
    constructor(shot) {
        super(shot);
        this.tasks = [];
    }

    _thenInitImpl() {
        return attract.api.thenGetTasks(this.getId())
        .then((response) => {
            this.tasks = response._items.map(t => new TaskRow(t));
            this.registerTaskEventListeners();

            return Promise.all(
                this.tasks.map(t => t.thenInit())
            );
        })
    }

    registerTaskEventListeners() {
        new TaskEventListener(this).register();
    }

    getTasksOfType(taskType) {
        return this.tasks.filter((t) => {
            return t.getProperties().task_type === taskType;
        })
    }

    getRowClasses() {
        let classes = super.getRowClasses()
        if(this.isInitialized) {
            classes['shot-not-in-edit'] = !this.underlyingObject.properties.used_in_edit;
        }
        return classes;
    }

    getChildObjects() {
        return this.tasks;
    }
}

export { ShotRow }
