let CellDefault = pillar.vuecomponents.table.cells.renderer.CellDefault;

const TEMPLATE =`
<div>
    <img
        v-if="img.src"
        :src="img.src"
        :alt="img.alt"
        :height="img.height"
        :width="img.width"
    />
    <generic-placeholder
        v-if="isLoading"
    />
</div>
`;

let CellPicture = Vue.component('pillar-cell-picture', {
    extends: CellDefault,
    template: TEMPLATE,
    data() {
        return {
            img: {},
            failed: false,
        }
    },
    computed: {
        isLoading() {
            if(!this.failed) {
                return !!this.rawCellValue && !this.img.src;
            }
            return false;
        }
    },
    created() {
        if (this.rawCellValue) {
            this.loadThumbnail(this.rawCellValue);
        }
    },
    watch: {
        rawCellValue(newValue) {
            this.loadThumbnail(newValue);
        }
    },
    methods: {
        loadThumbnail(imgId) {
            this.img = {};
            pillar.utils.thenLoadImage(imgId, 't')
            .then(fileDoc => {
                this.img = {
                    src: fileDoc.link,
                    alt: fileDoc.name,
                    width: fileDoc.width,
                    height: fileDoc.height,
                }
            }).fail(() => {
                this.failed = true;
            });
        }
    },
});

export { CellPicture }
