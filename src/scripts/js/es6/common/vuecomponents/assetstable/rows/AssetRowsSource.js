import { AttractRowsSourceBase } from '../../attracttable/rows/AttractRowsSourceBase'
import { AssetRow } from './AssetRow'

class AssetRowsSource extends AttractRowsSourceBase {
    constructor(projectId) {
        super(projectId, 'attract_asset', AssetRow);
    }

    thenGetRowObjects() {
        return attract.api.thenGetProjectAssets(this.projectId)
            .then((result) => {
                let assets = result._items;
                this.initRowObjects(assets);
            });
    }
}

export { AssetRowsSource }
