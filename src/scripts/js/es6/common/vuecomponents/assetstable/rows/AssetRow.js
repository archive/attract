import { AttractRowBase } from '../../attracttable/rows/AttractRowBase'
import { TaskEventListener } from '../../attracttable/rows/TaskEventListener';
import { TaskRow } from '../../taskstable/rows/TaskRow'

class AssetRow extends AttractRowBase {
    constructor(asset) {
        super(asset);
        this.tasks = [];
    }

    _thenInitImpl() {
        return attract.api.thenGetTasks(this.getId())
        .then((response) => {
            this.tasks = response._items.map(it => new TaskRow(it));
            this.registerTaskEventListeners();

            return Promise.all(
                this.tasks.map(t => t.thenInit())
            );
        });
    }

    registerTaskEventListeners() {
        new TaskEventListener(this).register();
    }

    getTasksOfType(taskType) {
        return this.tasks.filter((t) => {
            return t.getProperties().task_type === taskType;
        })
    }

    getChildObjects() {
        return this.tasks;
    }
}

export { AssetRow }
