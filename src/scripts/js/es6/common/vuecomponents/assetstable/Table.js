let PillarTable = pillar.vuecomponents.table.PillarTable;
import {AssetColumnFactory} from './columns/AssetColumnFactory'
import {AssetRowsSource} from './rows/AssetRowsSource'
import {RowFilter} from '../attracttable/rows/filter/RowFilter'

const TEMPLATE =`
<div class="pillar-table-actions">
    <button class="action"
        v-if="canAddAsset"
        @click="createNewAsset"
    >
        <i class="pi-plus">New Asset</i>
    </button>
</div>
`;

let TableActions = {
    template: TEMPLATE,
    computed: {
        canAddAsset() {
            let projectId = ProjectUtils.projectId();
            return attract.auth.AttractAuth.canUserCreateAsset(projectId);
        }
    },
    methods: {
        createNewAsset(event) {
            thenCreateAsset(ProjectUtils.projectUrl())
            .then((asset) => {
                this.$emit('item-clicked', event, asset._id);
            });
        }
    },
}

let AssetsTable = Vue.component('attract-assets-table', {
    extends: PillarTable,
    props: {
        project: Object
    },
    data() {
        return {
            columnFactory: new AssetColumnFactory(this.project),
            rowsSource: new AssetRowsSource(this.project._id),
            rowFilterConfig: {validStatuses: this.getValidStatuses()}
        }
    },
    methods: {
        getValidStatuses() {
            for (const it of this.project.node_types) {
                if(it.name === 'attract_asset'){
                    return it.dyn_schema.status.allowed;
                }
            }
            console.warn('Did not find allowed statuses for node type attract_shot');
            return [];
        }
    },
    components: {
        'pillar-table-actions': TableActions,
        'pillar-table-row-filter': RowFilter,
    }
});

export { AssetsTable };
