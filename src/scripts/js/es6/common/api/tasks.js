function thenGetTasks(parentId) {
    let where = {
        parent: parentId,
        node_type: 'attract_task'
    };
    return pillar.api.thenGetNodes(where);
}

function thenGetProjectTasks(projectId) {
    let where = {
        project: projectId,
        node_type: 'attract_task'
    }
    let embedded = {
        parent: 1
    }
    let sort = 'parent';
    return pillar.api.thenGetNodes(where, embedded, sort);
}

export { thenGetTasks, thenGetProjectTasks }
