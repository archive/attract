function thenGetProjectShots(projectId) {
    let where = {
        project: projectId,
        node_type: 'attract_shot'
    };
    let sort = '-properties.used_in_edit,properties.cut_in_timeline_in_frames';
    return pillar.api.thenGetNodes(where, {}, sort);
}

export { thenGetProjectShots }
