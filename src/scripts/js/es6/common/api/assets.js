function thenGetProjectAssets(projectId) {
    let where = {
        project: projectId,
        node_type: 'attract_asset'
    }
    return pillar.api.thenGetNodes(where);
}

export { thenGetProjectAssets }
