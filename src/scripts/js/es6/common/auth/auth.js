class ProjectAuth {
    constructor() {
        this.canCreateTask = false;
        this.canCreateAsset = false;
        this.canUseAttract = false;
    }
}

class Auth {
    constructor() {
        this.perProjectAuth = {}
    }

    canUserCreateTask(projectId) {
        let projectAuth = this.getProjectAuth(projectId);
        return projectAuth.canCreateTask;
    }

    canUserCreateAsset(projectId) {
        let projectAuth = this.getProjectAuth(projectId);
        return projectAuth.canCreateAsset;
    }

    canUserCanUseAttract(projectId) {
        let projectAuth = this.getProjectAuth(projectId);
        return projectAuth.canUseAttract;
    }

    setUserCanUseAttract(projectId, canUseAttract) {
        let projectAuth = this.getProjectAuth(projectId);
        projectAuth.canUseAttract = canUseAttract;
    }

    setUserCanCreateTask(projectId, canCreateTask) {
        let projectAuth = this.getProjectAuth(projectId);
        projectAuth.canCreateTask = canCreateTask;
    }

    setUserCanCreateAsset(projectId, canCreateAsset) {
        let projectAuth = this.getProjectAuth(projectId);
        projectAuth.canCreateAsset = canCreateAsset;
    }

    getProjectAuth(projectId) {
        this.perProjectAuth[projectId] = this.perProjectAuth[projectId] || new ProjectAuth();
        return this.perProjectAuth[projectId];
    }
}

let AttractAuth = new Auth();

export {AttractAuth}
