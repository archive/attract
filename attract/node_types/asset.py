node_type_asset = {
    'name': 'attract_asset',
    'description': 'Attract Asset Node Type, for planning asset creation',
    'dyn_schema': {
        'status': {
            'type': 'string',
            'allowed': [
                'on_hold',
                'todo',
                'in_progress',
                'review',
                'final'
            ],
            'default': 'todo',
            'required_after_creation': True,
        },
        'notes': {
            'type': 'string',
            'maxlength': 256,
        },
    },
    'form_schema': {},
    'parent': ['attract_scene', 'attract_shot']
}

task_types = ['layout', 'modeling', 'shading', 'rigging']
