import logging

from flask import Blueprint, render_template, request
import flask
import werkzeug.exceptions as wz_exceptions

import pillarsdk
import pillar.api.utils
from pillar import current_app
from pillar.web.projects.routes import project_navigation_links
from pillar.web.system_util import pillar_api

from attract.routes import attract_project_view
from attract.node_types.shot import node_type_shot
from attract import current_attract

from . import routes_common

perproject_blueprint = Blueprint('attract.shots.perproject', __name__,
                                 url_prefix='/<project_url>/shots')
log = logging.getLogger(__name__)


@perproject_blueprint.route('/', endpoint='index')
@perproject_blueprint.route('/with-task/<task_id>', endpoint='with_task')
@attract_project_view(extension_props=True)
def for_project(project, attract_props, task_id=None, shot_id=None):
    node_type_name = node_type_shot['name']

    shots, tasks_for_shots, task_types_for_template = routes_common.for_project(
        node_type_name,
        attract_props['task_types'][node_type_name],
        project, attract_props, task_id, shot_id)

    # Some aggregated stats
    stats = {
        'nr_of_shots': sum(shot.properties.used_in_edit or 0 for shot in shots),
        'total_frame_count': sum(shot.properties.duration_in_edit_in_frames or 0
                                 for shot in shots
                                 if shot.properties.used_in_edit),
    }
    can_use_attract = current_attract.auth.current_user_may(current_attract.auth.Actions.USE)
    navigation_links = project_navigation_links(project, pillar_api())
    extension_sidebar_links = current_app.extension_sidebar_links(project)

    selected_id = shot_id or task_id

    return render_template('attract/shots/for_project.html',
                           shots=shots,
                           tasks_for_shots=tasks_for_shots,
                           task_types=task_types_for_template,
                           selected_id=selected_id,
                           project=project,
                           attract_props=attract_props,
                           stats=stats,
                           can_use_attract=can_use_attract,
                           can_create_task=can_use_attract,
                           navigation_links=navigation_links,
                           extension_sidebar_links=extension_sidebar_links)


@perproject_blueprint.route('/<shot_id>')
@attract_project_view(extension_props=True)
def view_shot(project, attract_props, shot_id):
    if not request.is_xhr:
        return for_project(project, attract_props, shot_id=shot_id)

    shot, node_type = routes_common.view_node(project, shot_id, node_type_shot['name'])
    can_use_attract = current_attract.auth.current_user_may(current_attract.auth.Actions.USE)

    return render_template('attract/shots/view_shot_embed.html',
                           shot=shot,
                           project=project,
                           shot_node_type=node_type,
                           attract_props=attract_props,
                           can_use_attract=can_use_attract,
                           can_edit=can_use_attract and 'PUT' in shot.allowed_methods)


@perproject_blueprint.route('/<shot_id>', methods=['POST'])
@attract_project_view()
def save(project, shot_id):
    log.info('Saving shot %s', shot_id)
    log.debug('Form data: %s', request.form)

    if not current_attract.auth.current_user_may(current_attract.auth.Actions.USE):
        raise wz_exceptions.Forbidden()

    shot_dict = request.form.to_dict()
    current_attract.shot_manager.edit_shot(shot_id, **shot_dict)

    # Return the patched node in all its glory.
    api = pillar_api()
    shot = pillarsdk.Node.find(shot_id, api=api)
    return pillar.api.utils.jsonify(shot.to_dict())


# TODO: remove GET method once Pablo has made a proper button to call this URL with a POST.
@perproject_blueprint.route('/create', methods=['POST', 'GET'])
@attract_project_view()
def create_shot(project):
    if not current_attract.auth.current_user_may(current_attract.auth.Actions.USE):
        raise wz_exceptions.Forbidden()

    shot = current_attract.shot_manager.create_shot(project)

    resp = flask.make_response()
    resp.headers['Location'] = flask.url_for('.view_shot',
                                             project_url=project['url'],
                                             shot_id=shot['_id'])
    resp.status_code = 201
    return flask.make_response(flask.jsonify({'shot_id': shot['_id']}), 201)


@perproject_blueprint.route('/<shot_id>/activities')
@attract_project_view()
def activities(project, shot_id):
    if not request.is_xhr:
        return flask.redirect(flask.url_for('.view_shot',
                                            project_url=project.url,
                                            shot_id=shot_id))

    acts = current_attract.activities_for_node(shot_id)
    return flask.render_template('attract/shots/view_activities_embed.html',
                                 activities=acts)
