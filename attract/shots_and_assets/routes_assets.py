import logging

from flask import Blueprint, render_template, request
import flask

import pillarsdk
import pillar.api.utils
from pillar import current_app
from pillar.web.projects.routes import project_navigation_links
from pillar.web.system_util import pillar_api

from attract.routes import attract_project_view
from attract.node_types.asset import node_type_asset
from attract import current_attract

from . import routes_common

perproject_blueprint = Blueprint('attract.assets.perproject', __name__,
                                 url_prefix='/<project_url>/assets')
log = logging.getLogger(__name__)


@perproject_blueprint.route('/', endpoint='index')
@perproject_blueprint.route('/with-task/<task_id>', endpoint='with_task')
@attract_project_view(extension_props=True)
def for_project(project, attract_props, task_id=None, asset_id=None):
    can_use_attract = current_attract.auth.current_user_may(current_attract.auth.Actions.USE)
    navigation_links = project_navigation_links(project, pillar_api())
    extension_sidebar_links = current_app.extension_sidebar_links(project)
    selected_id = asset_id or task_id

    return render_template('attract/assets/for_project.html',
                           selected_id=selected_id,
                           project=project,
                           can_use_attract=can_use_attract,
                           can_create_task=can_use_attract,
                           can_create_asset=can_use_attract,
                           navigation_links=navigation_links,
                           extension_sidebar_links=extension_sidebar_links,
                           )


@perproject_blueprint.route('/<asset_id>')
@attract_project_view(extension_props=True)
def view_asset(project, attract_props, asset_id):
    if not request.is_xhr:
        return for_project(project, attract_props, asset_id=asset_id)

    asset, node_type = routes_common.view_node(project, asset_id, node_type_asset['name'])

    auth = current_attract.auth
    can_use_attract = auth.current_user_may(auth.Actions.USE)
    can_edit = can_use_attract and 'PUT' in asset.allowed_methods

    return render_template('attract/assets/view_asset_embed.html',
                           asset=asset,
                           project=project,
                           asset_node_type=node_type,
                           attract_props=attract_props,
                           can_use_attract=can_use_attract,
                           can_edit=can_edit)


@perproject_blueprint.route('/<asset_id>', methods=['POST'])
@attract_project_view()
def save(project, asset_id):
    log.info('Saving asset %s', asset_id)
    log.debug('Form data: %s', request.form)

    asset_dict = request.form.to_dict()
    current_attract.shot_manager.edit_asset(asset_id, **asset_dict)

    # Return the patched node in all its glory.
    api = pillar_api()
    asset = pillarsdk.Node.find(asset_id, api=api)
    return pillar.api.utils.jsonify(asset.to_dict())


@perproject_blueprint.route('/create', methods=['POST'])
@attract_project_view()
def create_asset(project):
    asset = current_attract.shot_manager.create_asset(project)

    resp = flask.make_response()
    resp.headers['Location'] = flask.url_for('.view_asset',
                                             project_url=project['url'],
                                             asset_id=asset['_id'])
    resp.status_code = 201
    return flask.make_response(flask.jsonify(asset.to_dict()), 201)


@perproject_blueprint.route('/<asset_id>/activities')
@attract_project_view()
def activities(project, asset_id):
    if not request.is_xhr:
        return flask.redirect(flask.url_for('.view_asset',
                                            project_url=project.url,
                                            asset_id=asset_id))
    acts = current_attract.activities_for_node(asset_id)

    # NOTE: this uses the 'shots' template, because it has everything we ever wanted.
    return flask.render_template('attract/shots/view_activities_embed.html',
                                 activities=acts)
