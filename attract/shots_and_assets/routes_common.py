import logging

import flask
import werkzeug.exceptions as wz_exceptions

import pillarsdk
from pillar.web.system_util import pillar_api
from pillar.web.utils import get_file
from pillar.auth import current_user

from attract import current_attract

log = logging.getLogger(__name__)


def for_project(node_type_name, task_types_for_nt, project, attract_props,
                task_id=None, shot_or_asset_id=None):
    """Common view code for assets and shots /attract/<project_url>/{assets,shots}"""
    api = pillar_api()

    found = pillarsdk.Node.all({
        'where': {
            'project': project['_id'],
            'node_type': node_type_name,
        },
        'sort': [
            ('properties.cut_in_timeline_in_frames', 1),
        ]
    }, api=api)
    nodes = found['_items']

    thumb_placeholder = flask.url_for('static_attract', filename='assets/img/placeholder.jpg')
    for node in nodes:
        picture = get_file(node.picture, api=api)
        if picture:
            node._thumbnail = next((var.link for var in picture.variations
                                    if var.size == 't'), thumb_placeholder)
        else:
            node._thumbnail = thumb_placeholder

        # The placeholder can be shown quite small, but otherwise the aspect ratio of
        # the actual thumbnail should be taken into account. Since it's different for
        # each project, we can't hard-code a proper height.
        node._thumbnail_height = '30px' if node._thumbnail is thumb_placeholder else 'auto'

    tasks_for_nodes = current_attract.shot_manager.tasks_for_nodes(nodes, task_types_for_nt)

    # Append the task type onto which 'other' tasks are mapped.
    task_types_for_template = task_types_for_nt + [None]

    return nodes, tasks_for_nodes, task_types_for_template


def view_node(project, node_id, node_type_name):
    """Returns the node if the user has access.
    """

    # asset list is public, asset details are not.
    if not current_user.has_cap('attract-view'):
        raise wz_exceptions.Forbidden()

    api = pillar_api()

    node = pillarsdk.Node.find(node_id, api=api)
    node_type = project.get_node_type(node_type_name)

    return node, node_type
