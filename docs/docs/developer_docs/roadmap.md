# Roadmap
The day-to-day planning for development is available on 
[developer.blender.org](https://developer.blender.org/project/board/72/). In this section we summarize
the high level goals for the projects.

## Self-provisionable Server
Make it possible for developers to run the full stack in a local environment. In similar way to 
Flamenco, the challenge is to get the Server (and its Pillar core) disconnected from Blender Cloud.

## Data filtering and sorting
Provide basic filtering and sorting functionality for assets, tasks, and assets. For example, make it
possible to find all tasks assigned to a specific user, or in a specific set of statuses.
