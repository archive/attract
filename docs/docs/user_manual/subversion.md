# Subversion Integration

By hooking your Subversion server to Attract, tasks can be automatically updated based on tags in
the commit message. This requires a post-commit hook to be installed on the Subversion server.

Example hook:

    REPOS="$1"
    REV="$2"
    TXN_NAME="$3"

    /usr/bin/python3 "$REPOS"/hooks/notify_attract.py "$REPOS" "$REV"

The
[`notify_attract.py`](https://developer.blender.org/source/attract/browse/master/notify_attract.py)
file is bundled with Attract's source code, and needs to be copied to the Subversion repository's
`hook` directory. After copying, modify the code to include an authentication token and the mapping from the Subversion repository name to the project URL. For example:

    AUTH_TOKEN = 'SRVxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
    PILLAR_URL = 'https://cloud.blender.org/'
    PROJECT_URLS = {  # Mapping from SVN repository name to Attract project URL.
        'svnreponame': 'p-123456789',
    }

The authentication token must be created on the server using `manage.py attract create_svner_account {emailaddress} {project-url}`

The project should have an `svn_url` extension property in the MongoDB database, which points to
the Subversion server URL. This is for logging use only.
