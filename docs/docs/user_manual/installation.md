# Installation & Configuration

!!! note
    This section of the manual is work in progress.

The following video shows how to set up a Blender Cloud project with Attract, which is currently
the only way officially supported. A step-by-step text version will follow.

<iframe width="750" height="350" src="https://www.youtube.com/embed/FoUua_Jlmpc?rel=0" frameborder="0" 
gesture="media" allow="encrypted-media" allowfullscreen></iframe>
