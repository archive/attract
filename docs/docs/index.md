# Attract Docs

Welcome to the Attract documentation pages! Here we collect both user and development docs. Attract
is the production tracking and management software used at Blender Animation Studio.

## Main features

* Shot list, asset list and task list
* Extensible design, supporting custom task types with custom attributes
* Integration of SVN activity in a task activity list
* Completely Free and Open Source software

## Status of the documentation

Documentation is an ongoing effort. We are currently focusing on user documentation, aimed at
Blender Cloud subscribers. If you are interested in installing Attract on your own infrastructure, 
consider checking out the sources and README.md files.
